﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Numerics;
using System.Security.Cryptography;
using System.IO;

namespace PrimeTest
{
    class Program
    {
        static bool MillerRabinTest(BigInteger n, int k)
        {
            // если n == 2 или n == 3 - эти числа простые, возвращаем true
            if (n == 2 || n == 3)
                return true;
            
            // если n < 2 или n четное - возвращаем false
            if (n < 2 || n % 2 == 0)
                return false;
            
            // представим n − 1 в виде (2^s)·t, где t нечётно, это можно сделать последовательным делением n - 1 на 2
            BigInteger t = n - 1;
            int s = 0;
            while (t % 2 == 0)
            {
                t /= 2;
                s += 1;
            }
            
            // повторить k раз
            for (int i = 0; i < k; i++)
            {
                // выберем случайное целое число a в отрезке [2, n − 2]
                RNGCryptoServiceProvider rng = new RNGCryptoServiceProvider();
                byte[] _a = new byte[n.ToByteArray().LongLength];
                BigInteger a;
                do
                {
                    rng.GetBytes(_a);
                    a = new BigInteger(_a);
                }
                while (a < 2 || a >= n - 2);
                
                // x ← a^t mod n, вычислим с помощью возведения в степень по модулю
                BigInteger x = BigInteger.ModPow(a, t, n);
                
                // если x == 1 или x == n − 1, то перейти на следующую итерацию цикла
                if (x == 1 || x == n - 1)
                    if (x == 1 || x == n - 1)
                    continue;

                // повторить s − 1 раз
                for (int r = 1; r < s; r++)
                {
                    // x ← x^2 mod n
                    x = BigInteger.ModPow(x, 2, n);
                    
                    // если x == 1, то вернуть "составное"
                    if (x == 1)
                        return false;
                    
                    // если x == n − 1, то перейти на следующую итерацию внешнего цикла
                    if (x == n - 1)
                        break;
                }
                if (x != n - 1)
                    return false;
            }
            return true;
        }

        /*static void FromString(byte[] _B)
        {
            StreamReader sr = new StreamReader("primes2.txt");
            if (sr == null)
                Console.WriteLine("Enable to open file");
            string line;
            while ((line = sr.ReadLine()) != null)
            {
                byte[] arr = new byte[128];
                byte[] ba = Encoding.Default.GetBytes(line);
                for (int j = 0; j < ba.Length; j++)
                {
                    if (ba[j] <= 0x60)
                        ba[j] = (byte)(ba[j] - 0x30);
                    if (ba[j] == 0x61)
                        ba[j] = 0x0a;
                    if (ba[j] == 0x62)
                        ba[j] = 0x0b;
                    if (ba[j] == 0x63)
                        ba[j] = 0x0c;
                    if (ba[j] == 0x64)
                        ba[j] = 0x0d;
                    if (ba[j] == 0x65)
                        ba[j] = 0x0e;
                    if (ba[j] == 0x66)
                        ba[j] = 0x0f;
                }
                for (int i = 0; i < 128; i++)
                    arr[i] = (byte)(((ba[i * 2] - 0x30) << 4) | (ba[i * 2 + 1] - 0x30));
                Console.WriteLine(line);
                Console.WriteLine();
                _B = arr.Reverse().ToArray();
            }
        }*/
        static void Main(string[] args)
        {
            
            int rnd = 10000;
            StreamReader sr = new StreamReader("primes2.txt");
            if (sr == null)
                Console.WriteLine("Enable to open file");
            string line;
            while ((line = sr.ReadLine()) != null)
            {
                byte[] arr = new byte[128];
                byte[] ba = Encoding.Default.GetBytes(line);
                for (int j = 0; j < ba.Length; j++)
                {
                    if (ba[j] <= 0x60)
                        ba[j] = (byte)(ba[j] - 0x30);
                    if (ba[j] == 0x61)
                        ba[j] = 0x0a;
                    if (ba[j] == 0x62)
                        ba[j] = 0x0b;
                    if (ba[j] == 0x63)
                        ba[j] = 0x0c;
                    if (ba[j] == 0x64)
                        ba[j] = 0x0d;
                    if (ba[j] == 0x65)
                        ba[j] = 0x0e;
                    if (ba[j] == 0x66)
                        ba[j] = 0x0f;
                }
                for (int i = 0; i < 128; i++)
                    arr[i] = (byte)(((ba[i * 2]) << 4) | (ba[i * 2 + 1]));
                Console.WriteLine(line);
                Console.WriteLine();
                byte[] reversed = arr.Reverse().ToArray();
                BigInteger I = new BigInteger(reversed);
                Console.WriteLine("{0:x}", I);
                if (MillerRabinTest(I, rnd))
                {
                    Console.WriteLine("Test passed!");
                    Console.WriteLine();
                }
                else
                {
                    Console.WriteLine("Test not passed!");
                    Console.WriteLine();
                }
            }

            sr.Close();



            /*BigInteger input = 16922038198074260747; // 346675468568;
            int rnd = 100;
            if (MillerRabinTest(input, rnd))
                Console.WriteLine("Test passed!");
            else
                Console.WriteLine("Test not passed!");
            byte[] newarr = new byte[128];
            for (int i = 0; i < 128; i++)
            {
                newarr[i] = (byte)i;
            }
            BigInteger I = new BigInteger(newarr);
            Console.WriteLine("{0:x}",I);
            if (MillerRabinTest(I, rnd))
                Console.WriteLine("Test passed!");
            else
                Console.WriteLine("Test not passed!");
            Random rand = new Random();*/
        }
    }
}
